

// Gestion de la couleur des boutons

function buttonColorMale() {
    document.getElementById('male').style.backgroundColor='#5bc0de';
    document.getElementById('male').style.color='#fff';
    maleTrue = true;
    nombreTotal();
}

function buttonColorFemale() {
    document.getElementById('female').style.backgroundColor='#5bc0de';
    document.getElementById('female').style.color='#fff';
    femaleTrue = true;
    nombreTotal();
}

function buttonColorReset() {
    document.getElementById('female').style.backgroundColor='#F9F9F9';
    document.getElementById('female').style.color='#000';
    document.getElementById('male').style.backgroundColor='#F9F9F9';
    document.getElementById('male').style.color='#000';
    document.getElementById('5km').style.backgroundColor='#F9F9F9';
    document.getElementById('5km').style.color='#000';
    document.getElementById('510km').style.backgroundColor='#F9F9F9';
    document.getElementById('510km').style.color='#000';
    document.getElementById('10km').style.backgroundColor='#F9F9F9';
    document.getElementById('10km').style.color='#000';
    maleTrue = false;
    femaleTrue = false;

    var machin = document.getElementsByClassName("reset");
    for (i = 0; i < 23; i++) {
        machin[i].style.opacity = "1.0";
    }

    nombreTotal();
}
buttonColorReset();

// Gestion des filtres

function genderMale() {
    var truc = document.getElementsByClassName("male");
    for (i = 0; i < 13; i++) {
        truc[i].style.border = "1px #4FC37D solid";
    }
}

function genderFemale() {
    var truc = document.getElementsByClassName("female");
    for (j = 0; j < 10; j++){
        truc[j].style.border = "1px #4FC37D solid";
    }
}

function genderReset() {
    var truc = document.getElementsByClassName("reset");
    for (k = 0; k < 23; k++){
        truc[k].style.border = "none";
    }
}

//Cacul du nombre d'apprendant

function nombreMasculin() {
    var nbMasculin;
    nbMasculin = document.getElementsByClassName("male").length;
    console.log(nbMasculin);
}

function nombreFeminin() {
    var nbFeminin;
    nbFeminin = document.getElementsByClassName("female").length;
    console.log(nbFeminin);
}

function nombreTotal() {
    var nbTotal;

    if (maleTrue == true && femaleTrue == true) {
        nbTotal = 23;
    } else if (maleTrue == false && femaleTrue == true) {
        nbTotal = 10;
    } else if (maleTrue == true && femaleTrue == false) {
        nbTotal = 13;
    } else if (maleTrue == false && femaleTrue == false) {
        nbTotal = 23;
    } else {
        nbTotal = None;
    }
    var text = document.getElementById("divA").textContent;
    document.getElementById("divA").textContent = nbTotal;
    return(nbTotal);
}

// Boutons de distance

function button5km() {
    document.getElementById('5km').style.backgroundColor = '#5bc0de';
    document.getElementById('5km').style.color = '#fff';
}

function button510km() {
    document.getElementById('510km').style.backgroundColor = '#5bc0de';
    document.getElementById('510km').style.color = '#fff';
}

function button10km() {
    document.getElementById('10km').style.backgroundColor = '#5bc0de';
    document.getElementById('10km').style.color = '#fff';
}

// Filtre distance

function filtre5km() {

    document.getElementById('510km').style.backgroundColor='#F9F9F9';
    document.getElementById('510km').style.color='#000';
    document.getElementById('10km').style.backgroundColor='#F9F9F9';
    document.getElementById('10km').style.color='#000';

    var truc = document.getElementsByClassName("reset");
    for (i = 0; i < 23; i++) {
        truc[i].style.opacity = "0.4";
    }
    var machin = document.getElementsByClassName("5km");
    for (i = 0; i < 23; i++) {
        machin[i].style.opacity = "1.0";
    }

}

function filtre510km() {

    document.getElementById('5km').style.backgroundColor='#F9F9F9';
    document.getElementById('5km').style.color='#000';
    document.getElementById('10km').style.backgroundColor='#F9F9F9';
    document.getElementById('10km').style.color='#000';

    var truc = document.getElementsByClassName("reset");
    for (i = 0; i < 23; i++) {
        truc[i].style.opacity = "0.4";
    }
    var machin = document.getElementsByClassName("510km");
    for (i = 0; i < 23; i++) {
        machin[i].style.opacity = "1.0";
    }

}

function filtre10km() {

    document.getElementById('5km').style.backgroundColor='#F9F9F9';
    document.getElementById('5km').style.color='#000';
    document.getElementById('510km').style.backgroundColor='#F9F9F9';
    document.getElementById('510km').style.color='#000';

    var truc = document.getElementsByClassName("reset");
    for (i = 0; i < 23; i++) {
        truc[i].style.opacity = "0.4";
    }
    var machin = document.getElementsByClassName("10km");
    for (i = 0; i < 23; i++) {
        machin[i].style.opacity = "1.0";
    }

}
